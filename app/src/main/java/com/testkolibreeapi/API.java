package com.testkolibreeapi;

import android.os.AsyncTask;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by IMiyke on 12/04/2015.
 */
public class API extends AsyncTask<Void, Integer, String>
{
    public interface OnRequestListener
    {
        void finish(String result);
    }

    private OnRequestListener callback;
    private ArrayList<NameValuePair> args;

    public API(OnRequestListener callback)
    {
        this.callback = callback;

        args = new ArrayList<>();
        args.add(new BasicNameValuePair("email", MainActivity.email));
        args.add(new BasicNameValuePair("password", MainActivity.Pass));
        this.execute();
    }


    @Override
    protected String doInBackground(Void... params)
    {

        return Internet.doRequest("/v1/accounts/request_token/", args);
    }

    @Override
    protected void onPostExecute(String s)
    {
//        super.onPostExecute(s);
        callback.finish(s);
    }
}
