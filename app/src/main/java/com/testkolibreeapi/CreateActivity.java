package com.testkolibreeapi;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;


public class CreateActivity extends ActionBarActivity {

    public Button ButtonRegister=null;
    static String first_name;
    static String last_name;
    static String gender;
    static String survey_handedness;
    static String birthday;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        ButtonRegister= (Button)findViewById(R.id.ButtonRegister);



        final TextView textView27 = (TextView) findViewById(R.id.textView27);

            ButtonRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                first_name=((EditText)findViewById(R.id.editText_fn)).getText().toString();
                last_name=((EditText)findViewById(R.id.editText_ln)).getText().toString();
                gender=((EditText)findViewById(R.id.editText_gender)).getText().toString();
                survey_handedness=((EditText)findViewById(R.id.editText_hand)).getText().toString();
                birthday=((EditText)findViewById(R.id.editText_birth)).getText().toString();
                new API2(new API2.OnRequestListener()
                {
                    @Override
                    public void finish(String result)
                    {

                        Bundle b=new Bundle();
                        try {
                            JSONObject jobj = new JSONObject(result);
                            result=jobj.getString("picture");
                            result="Thanks, your profile is coming!";


                        } catch (JSONException e) {
                            e.printStackTrace();
                            result="ERROR while registering";
                        }
                        b.putString("result",result);
                        textView27.setText(result);



                    }
                });
         };



            });
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
