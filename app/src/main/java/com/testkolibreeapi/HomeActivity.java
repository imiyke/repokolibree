package com.testkolibreeapi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class HomeActivity extends ActionBarActivity {

    Bundle b;
    static String str = null;
    static JSONObject jobj = null;
    static String token,account;
    Button ButtonCreate=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        b = getIntent().getExtras();
        str = b.getString("result");


        JSONArray objeeet;

        ButtonCreate= (Button)findViewById(R.id.Button_create);


        try {
            jobj = new JSONObject(str);
            account=jobj.getString("id");
            objeeet = jobj.getJSONArray("profiles");
            token = jobj.getString("access_token");
            onDrawProfiles(objeeet);

        } catch (JSONException e) {
            if (ButtonCreate.isEnabled())
                ButtonCreate.setEnabled(false);
            e.printStackTrace();
        }

        ButtonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CreateActivity.class);

                startActivity(intent);


            }
        });

    }



    public void onDrawProfiles(JSONArray JSob) {
        try {



            ArrayList<ListView> ArrayTextview = new ArrayList<>(JSob.length());
            LinearLayout lineaLay = (LinearLayout) findViewById(R.id.linear27);
            lineaLay.setOrientation(LinearLayout.VERTICAL);
            for (int i = 0; i < JSob.length(); i++) {
                ArrayList<String> Strings = new ArrayList<>(6);
                JSONObject JSoobj = JSob.getJSONObject(i);
                Strings.add("Profile: "+Integer.toString(i+1));
                Strings.add("First Name: "+JSoobj.getString("first_name"));
                Strings.add("Last Name: "+JSoobj.getString("last_name"));
                Strings.add("Gender: "+JSoobj.getString("gender"));
                Strings.add("Survey handedness: "+JSoobj.getString("survey_handedness"));
                Strings.add("");

                ArrayTextview.add(new ListView(this));
                lineaLay.addView(ArrayTextview.get(i));

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,Strings);
                ArrayTextview.get(i).setAdapter(adapter);
                justifyListViewHeightBasedOnChildren(ArrayTextview.get(i));
                lineaLay.setSystemUiVisibility(1);

            }
        } catch (JSONException e) {
            e.printStackTrace();
            ButtonCreate.setEnabled(false);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void justifyListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }
}
