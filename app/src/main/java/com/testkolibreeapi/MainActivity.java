package com.testkolibreeapi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity
{
    Button Button_submit=null;
    static API firstAPI;
    static String email,Pass,secret,clienID;
     String testt;
    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button_submit= (Button)findViewById(R.id.Button_submit);



        Button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email=((EditText)findViewById(R.id.edit_mail)).getText().toString();
                Pass=((EditText)findViewById(R.id.edit_mdp)).getText().toString();
                secret=((EditText)findViewById(R.id.edit_accPass)).getText().toString();
                clienID=((EditText)findViewById(R.id.edit_ID)).getText().toString();
             firstAPI=   new API(new API.OnRequestListener()
                {
                    @Override
                    public void finish(String result)
                    {

                        try {
                            JSONObject jobj = new JSONObject(result);
                            testt=jobj.getString("profiles");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            testt="";
                        }
                        if (testt=="")
                        {
                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                            startActivity(intent);

                        }
                        else
                        {
                        Bundle b=new Bundle();
                        b.putString("result",result);

                        Intent intent=new Intent(MainActivity.this,HomeActivity.class);
                        intent.putExtras(b);
                        startActivity(intent);
                    }
                    }
                });



            }
        });

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }







    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        int id = item.getItemId();


        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
