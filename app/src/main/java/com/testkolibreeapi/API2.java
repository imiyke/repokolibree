package com.testkolibreeapi;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by IMiyke on 15/04/2015.
 */
public class API2 extends AsyncTask<Void, Integer, String> {
    public interface OnRequestListener
    {
        void finish(String result);
    }

    private OnRequestListener callback2;

    JSONObject jsonobj = new JSONObject();

    public  API2(API2.OnRequestListener callback2)
    {
        this.callback2=  callback2;


        try {
            jsonobj.put("first_name",CreateActivity.first_name);
            jsonobj.put("last_name", CreateActivity.last_name);
            jsonobj.put("gender", CreateActivity.gender);
            jsonobj.put("survey_handedness",CreateActivity.survey_handedness);
            jsonobj.put("birthday",CreateActivity.birthday);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        this.execute();

    }

    @Override
    protected String doInBackground(Void... params)
    {

        return Internet.doRequestCreate("/v1/accounts/474/profiles/", jsonobj);
    }

    @Override
    protected void onPostExecute(String s)
    {
//        super.onPostExecute(s);
        callback2.finish(s);
    }
}

