package com.testkolibreeapi;

import android.util.Base64;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class Internet
{
    private static String API_URL = "https://test.api.kolibree.com";

    public static String doRequest(String partUrl, ArrayList<NameValuePair> args)
    {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(API_URL + partUrl);
        post.addHeader("http-x-client-id",MainActivity.clienID);
        post.addHeader("http-x-client-sig", hash_client_secret(MainActivity.secret));

        StringBuilder sb = new StringBuilder();
        try
        {
            post.setEntity(new UrlEncodedFormEntity(args));
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = "";
            while ((line = rd.readLine()) != null)
                sb.append(line).append("\n");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }
    public static String doRequestCreate(String partUrl, JSONObject args)
    {

        HttpPost Post = new HttpPost(API_URL + partUrl);
        Post.addHeader("http-x-client-id",MainActivity.clienID);
        Post.addHeader("http-x-client-sig", hash_client_secret2(MainActivity.secret));
        Post.addHeader("http-x-access-token",HomeActivity.token);


        StringBuilder sb = new StringBuilder();
        try
        {   StringEntity params =new StringEntity(args.toString());
            params.setContentType("application/json;charset=UTF-8");
            params.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8")) ;

            HttpClient client = new DefaultHttpClient();


            Post.setEntity(params);
            HttpResponse httpResponse = client.execute(Post);


            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            String line = "";
            while ((line = rd.readLine()) != null)
                sb.append(line).append("\n");

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return sb.toString();

    }


    /**
     * function is used to has the client signature header *
     */
    private static String hash_client_secret(String client_secret)
    {
        String hash = null;
        try
        {
            String url = API_URL;
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(client_secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            //TODO: Faut faire marcher ca
            hash = Base64.encodeToString(sha256_HMAC.doFinal((url+"/v1/accounts/request_token/").getBytes()), Base64.NO_WRAP);
        }
        catch (Exception e)
        {
            System.out.println("Error");
        }
        return hash;
    }
    private static String hash_client_secret2(String client_secret)
    {
        String hash2 = null;
        try
        {
            String url = API_URL;
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(client_secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            //TODO: Faut faire marcher ca
            hash2 = Base64.encodeToString(sha256_HMAC.doFinal((url+"/v1/accounts/"+HomeActivity.account+"/profiles/").getBytes()), Base64.NO_WRAP);
        }
        catch (Exception e)
        {
            System.out.println("Error");
        }
        return hash2;
    }
}
